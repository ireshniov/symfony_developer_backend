<?php

namespace SD\UserBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository
 * @package SD\UserBundle\Entity\Repository
 */
class UserRepository extends EntityRepository
{
}