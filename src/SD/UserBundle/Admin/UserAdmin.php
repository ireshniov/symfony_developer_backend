<?php

namespace SD\UserBundle\Admin;

use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\UserBundle\Admin\Model\UserAdmin as BaseAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class UserAdmin extends BaseAdmin
{

    /**
     * @var string
     */
    protected $baseRouteName = 'admin_user';

    /**
     * @var string
     */
    protected $baseRoutePattern = '/user';

    /**
     * @var string
     */
    protected $translationDomain = 'admin';

    protected $formOptions = [
        'validation_groups' => []
    ];

    /**
     * {@inheritdoc}
     */
    public function getFormBuilder()
    {
        $this->formOptions['data_class'] = $this->getClass();

        $formBuilder = $this->getFormContractor()->getFormBuilder(
            $this->getUniqid(),
            $this->formOptions
        );

        $this->defineFormBuilder($formBuilder);

        return $formBuilder;
    }

    /**
     * Fields to be shown on create/edit forms
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $container = $this->getConfigurationPool()->getContainer();

        if ($container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            $formMapper
                ->add('username','text',[
                    'required' => false,
                ])
                ->add('email','text',[
                    'required' => false,
                ])
                ->add('plainPassword', 'repeated', [
                        'type' => 'password',
                        'first_options' => ['label' => 'Password'],
                        'second_options' => ['label' => 'Password confirmation'],
                        'invalid_message' => 'fos_user.password.mismatch',
                        'required'    => false,
                    ]
                )
            ;
        }
    }

    /**
     * @param ErrorElement $errorElement
     * @param mixed $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {

        $errorElement
            ->with('username')
            ->assertNotBlank()
            ->assertLength(['min' => 1, 'max' => 100])
            ->assertRegex(['pattern' => "/^([А-Яа-яёЁa-zA-Z \-]+)$/iu"])
            ->end()
            ->with('email')
            ->assertNotBlank()
            ->assertEmail()
            ->end()
            ->with('plainPassword')
            ->assertNotBlank()
            ->assertLength(['min' => 6, 'max' => 100])
            ->assertRegex(['pattern' => "/((?=.*\d)(?=.*[А-Яа-яёЁa-zA-Z]))/iu"])
            ->end()
        ;
    }

    /**
     * Fields to be shown on filter forms
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username')
            ->add('email')
            ->add('enabled');
    }

    /**
     * Fields to be shown on lists
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username')
            ->add('email')
            ->add('enabled', null, ['editable' => true])
            ->add('lastLogin', null, ['format' => 'd-m-Y H:i'])
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('username')
            ->add('email')
            ->add('enabled', null, ['editable' => true])
            ->add('lastLogin', null, ['format' => 'd-m-Y H:i'])
        ;
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
    }
}
